# Manuscript folder

This directory contains figures, manuscript files, and supplemental
data files.


* * *

# What's here

* PollenSplicing.docx - first draft submitted to PeerJ, formatted as per specs on the Web site 
* PollenAS-ChangesTracked.docx - another, later draft, edited to include suggestions from reviewers. This version stopped working with EndNote and also kept crashing MS Word.
* PollenAS-ChangesTracked.doc - made from PollenAS-ChangesTracked.docx. This version kept crashing MS Word.
* PollenAS-PeerJ.docx - made from PollenAS-ChangesTracked.doc, reformatted, changes accepted and track changes turned off. This version also includes all the figures to ensure reviewers will see all the figures and legends on the same page.
* ResponseToReviews.docx - contains reviewers suggestions for improvement and how we used them to improve the manuscript.
* PrimaryFigures_Table_SuppFigures.docx - Word document with figures and figure legends and table
* peerj_reviewing_554_Loraine.pdf - reviewers copy for second round of reviews

* * *

## PSD 

Contains figure files in Photoshop format.

* * *

## SupplementaryFiles

Contains supplemental data files. SupplementalFile 1 contains the list
of most differentially spliced events between leaf and
pollen. SupplementalFile2, which we did not include with the
manuscript, contains some code and results files from merging
expression and splicing results. See the Markdown file for details.


* * * 

## gelimages

Contains digital photographs of gels. Some are annotated from analysis
software.

* * *

## igbpics

Contains images from Integrated Genome Browser.

* * *

## png

Contains figure files in PNG format.



