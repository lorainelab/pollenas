#!/usr/bin/env python

import sys
import glob
import os.path
import csv

"""This script is meant to take the several files of sequence data that we get from Eton and combine them into a single fasta file. Run this program in the directory with the sequence files, and a Codebook.csv file, with the desired name of the new compiled fasta file as the only argument.  All .seq files in the directory will be combined into one fasta file.

The full process is 4 steps:
1- Put sequence files into one folder
2- Create Codebook.csv with the file names in the first column and the desired names in the second
2- Run this script
3- Run blat

to run blat, use
$ blat genome fasta -maxIntron=2000 -t=dna -minIdentity=95 -noHead PSLout
genome = /Users/lorainelabfive/data/genomes/pub/quickload/A_thaliana_Jun_2009/A_thaliana_Jun_2009.2bit
fasta = file made by this script
PSLout = Whatever you'd like to call the output, probably with .pslx

The location of the genome file may be different on different computers.

"""



def main(Fasta):
    with open("Codebook.csv", mode='rU') as f:
        reader = csv.reader(f)
        dict = {rows[0]:rows[1] for rows in reader}
    NewFa=open(Fasta, 'w')
    ListFiles=glob.glob('*.seq')
    for seqFile in ListFiles:
        seqFile=seqFile.rstrip('\n')
        nickName=dict[seqFile]
        addToFasta(seqFile, nickName, NewFa)
    NewFa.close()
    print Fasta+" should now be ready to view."+"\n"


def addToFasta(seqFile, nickName, NewFa):
    NewFa.write(">"+nickName+" "+seqFile+"\n")
    fh=open(seqFile, 'r')
    for text in fh:
        seq=text.rstrip('\n')#when I open the Eton files as text, the text is broken by new-lines.
        NewFa.write(seq)
    fh.close()
    NewFa.write("\n")#so there is a new line before the next entry.
    print seqFile+" has been added to the fasta file."



# main block
if __name__=='__main__':
    Fasta = sys.argv[1]
    
    main(Fasta)
