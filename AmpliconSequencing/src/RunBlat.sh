#!/bin/sh

#  RunBlat.sh
#  
#
#  Created by Ivory Blakley on 1/13/15.
#

genome="/Users/lorainelab/Documents/EtonSequences/A_thaliana_Jun_2009.2bit"
fasta="../results/Amplicon.fa"
PSLout="Amplicon.pslx"

export PATH=$PATH:~/Documents/EtonSequences/software

blat $genome $fasta -maxIntron=2000 -t=dna -minIdentity=95 -noHead $PSLout


### End