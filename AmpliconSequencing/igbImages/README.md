# IGB images

This directory contains images made using Integrated Genome Browser.

To view the same scenes interactively, launch IGB and select the
Arabidopsis June 2009 genome assembly. Open the data source labeled
"pollen" in the Data Access panel to load and view data sets.

**Note** Tracks labeled "Seedling1" or "Seedling2" are the same as tracks named "L1" and "L2" in the figures.