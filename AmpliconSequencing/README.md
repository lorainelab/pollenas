# Results from amplicon sequencing

This directory contains amplicon sequence data, provided by Eton
Bioscience.

PCR was done using primers used for gel analysis, amplicons (bands)
were separated by gel electrophoresis, bands were excised and
extracted from the gels and then sent for sequencing.

* * * 

# What's here

## AmpliconSequencingResults.xlsx

Summary of amplicon sequence results. Also contains naming details,
names of primers used, and other information. 

* * * 

## data

Contains sequence files (extension .seq) obtained from Eton. Each
file contains one sequence obtained from sequencing an amplicon. 

Sequence files are named according to Eton naming scheme. The original
names also appear in AmpliconSequencingResults.xlsx.

Codebook.csv lists the name of each .seq file along with the 
sequence name following ournaming convention. This file is used by 
CompileFasta.py to assign meaningful names in to the fasta sequence.

* * * 

## results

Contains:

* Amplicon.fa - single fasta file containing sequences.
* Amplicon.pslx - output of aligning Amplicion.fa onto the Arabidopsis genome

**Note**: target genome sequence in 2bit format (used by blat) is available from the [IGB QuickLoad site Arabidopsis directory](http://igbquickload.org/quickload/A_thaliana_Jun_2009).

* * *

## src

Scripts for running blat and compiling .seq files into a single
fasta format file. 

* * * 

## igbImages

Contains images showing alignments of amplicon sequences alongside
Arabidopsis gene models. Typically, sequencing was done for leaf and
pollen samples, when enough amplicon material was obtained. Some
splice variants were such low abundance that it was impractical to
obtain enough material for sequencing.

* * *

# Amplicon naming convention

To distinguish sequences, we renamed each sequence obtained from Eton 
accroding to its target locus, its observed amplicion size, the source
cDNA, the primer used in sequencing, and a unique number in cases when
we obtained multiple sequences per amplicon. 

Each sequence was named as:

[Locus id]-[Observed amplicon size]-[L or P]-[F or R]-[N] where N is a
number.

The fasta headers in Amplicon.fa contain the sequence name followed by
a space, followed by Eton file name.

* * * 

# Aligning sequences

Sequences were aligned onto the Arabidopsis genome using blat, by Jim
Kent. 

Parameters for running blat are in the the script runBlast.sh, in the
src directory.

The output was a pslx format file, which contains details of the alignment
as well as the original query sequence. This enables visualization of
the query sequence together with mismatches, insertions, and deletions.

* * * 

# Visualizing amplicon alignments

The easiest way to visualize the alignments is to open the blat output
pslx file in Integrated Genome Browser. (Other browsers can probably 
also open this file, but we have not tested them.)

To obtain and run a copy of Integrated Genome Browser (IGB)

* Visit the [BioViz Web site](http://www.bioviz.org)
* Click the **Download** link
* Download the IGB installer for your platform
* Double-click the installer and follow the directions.

To view the pslx (blat output) file in IGB:

* Launch IGB (see above)
* Click-drag the file directly into the IGB main window *or*
* Choose **File > Open** and choose the file.

A new track named for the file will appear. To load the data, click
the **Load Data** button.

**Tip**: This is a small file and you can load all the data at
  once. To load all the data at once, open the **Data Access** tab and
  select **Load Mode > Genome**.

Understanding the results will be easier if you use the IGB **Color by**
feature to color-code alignments by source, i.e., whether they come
from pollen or leaf cDNA.

To color-code the alignments by their source:

* In IGB, right-click the track labeled **Amplicon.pslx**
* Select **Color by**; a new window will open
* Select **Property > id** 
* Enter **L** (for Leaf)
* Click the **match** color swatch and select a color to represent leaf samples
* Click the **not match** color swatch to represent the other (pollen) samples

The supplemental figures in the paper are using the following colors for pollen and leaf:

* Pollen:  R:145, G:25, B:33 (dark red)
* Leaf:  R:0, G:68, B:252 (blue)

