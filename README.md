# Analysis of alternative splicing in Arabidopsis pollen

The files contained in this repository describe data analyses
investigating pollen-specific splicing patterns in Arabidopsis. The
work presented is also described in an article appearing in 
open access journal PeerJ. 

See: 

[Analysis of pollen-specific alternative splicing in Arabidopsis thaliana via semi-quantitative PCR](https://peerj.com/articles/919/).

To use this repository, we recommend you clone it using
git. Alternatively, you can download the entire contents as a single
zip file. To download, click the download link at right. (Looks like a
cloud.)

## Questions? Comments?

Contact:

* Ann Loraine aloraine@uncc.edu
* April Estrada april.roberts@uncc.edu
* Nowlan Freese nfreese@uncc.edu
* Ivory Blakley ivoryec@gmail.com

* * * 

# What's here

To browse the contents of this repository, click the **Source** icon
(looks like a page, top left) and follow links to view or download
individual files.

* * *

## GelAnalysis

Contains contains R Markdown (.Rmd) files describing analysis of
digitized gel images of PCR products.

To run these yourself, clone this repo to your local computer and then
open the R project file *GelAnalysis.Rproj* in RStudio. Use knitr (in
RStudio) to knit each .Rmd file.

* * * 

## AmpliconSequence

Contains results from Sanger sequencing PCR amplicons, including sequence
data and visualizations of sequence alignments in [Integrated Genome Browser](http://bioviz.org/igb).

* * * 

## Manuscript

Contains manuscript drafts, figure files, reviewer comments, and supplemental
data files.

* * * 

# License 

Copyright (c) 2015 University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT