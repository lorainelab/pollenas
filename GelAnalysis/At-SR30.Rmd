```{r echo=FALSE}
g='AT1G09140'
symbol='At-SR30'
```


Tissue-specific alternatve splicing of `r symbol`
==============================

Introduction
------------

We used semi-quantitative PCR to assay differential splicing of `r symbol` in pollen and leaves.

This Markdown document aims to answer the question:

* Did semi-quantitative PCR confirm pollen-specific splicing for `r symbol`?

Analysis
--------

Load functions for reading, manipulating data, creating plots:

```{r}
source('src/GelAnalysisFunctions.R')
suppressPackageStartupMessages(library("xlsx"))
```

Read spreadsheet with data from gel, add percentage, make plot:

```{r fig.height=5,fig.width=5}
f=paste0('data/',g,'/sr302nd.51secexpBKCT.xls')
dat=read.xlsx2(f,2,header=T,
                colClasses=c('character','numeric','character',
                             'numeric'))
dat$per=calcPercentage(dat)
makeBarplot(dat,symbol)
fname=paste0('results/',symbol,'-',g,'.tiff')
quartz(file=fname,height=5,width=5,dpi=600,type="tiff")
makeBarplot(dat,symbol)
dev.off()
```

Test whether there's a difference using a t test. 

```{r}
testPer(dat)
```

Conclusion
----------

The relative abundance of splice variants was different in pollen and leaf (seedling) cDNA. 
